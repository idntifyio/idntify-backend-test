![Idntify Logo](https://cdn.idntify.io/idntify-logo.png)

# Sr/Mid Backend Developer Test

Idntify is launching a new product, as their project manager states, the backend is more important than the frontend for this new product, also, their clients are eager to try out their new API. Your job is to create this production-grade API.

---

Their new product is a database of animals and their owners, where owners can share their photos with other users as long as they are friends. You can use a database or keep the data in memory.
The API must expose the following methods

### API
1. Register a user. The user must be able to register using an email and a password
2. The authenticated user should be able to log in. No need for a strong authorization/authentication strategy is required, as long as the methods marked with a **( P )** at the end require the user to be authenticated
3. The authenticated user can logout **( P )**
4. The authenticated user must be able to upload a photo, can be base64 or multipart. When uploading the photo, a tag must be provided, something like "DOG", or "CAT". Only a few values are allowed. **( P )**
5. The authenticated user must be able to delete a photo (of their own). **( P )**
6. The authenticated user must be able to see a list of possible friends by searching a tag (other people with photos with "DOG" tags). **( P )**
7. The authenticated user can add a friend by providing their userid **( P )**
8. The authenticated user can delete a friend by providing their userid **( P )**
9. The authenticated user can send a message to another user. **( P )**
10. The authenticated user can see a list of photos from another user. **( P )**
11. The authenticated user can retrieve a photo of their own. **( P )**
12. The authenticated user can retrieve a photo of their friends. **( P )**
13. The authenticated user can update their profile with personal information (phone, name, lastname, username, opt-out) **( P )**
14. The authenticated user can retrieve personal info from a friend (phone, name, lastname, username) **( P )**

--- 

#### Notes on API
- All users have an inbox. A user can send another user a message as long as they are **BOTH** friends. Messages are limited to a 255 character string. This inbox must be persisted.
- If a user adds another user, the user must receive "an invitation" from the requester user. The user then can proceed to use the method to add a friend given their userId.
- A user should not be able to see the list of photos from another user if they are not friends
- If the user updates their info with the opt-out, they should not appear on the search of friends based on tags
- If the user updates their info with the opt-out, their personal info should not be retrieved by the personal info retrieval endpoint

#### Bonus Points For:
- Tests, the more coverage the better
- Data validation, good error handling / messages / http statuses
- Comments, a good readme, instructions, etc.
- Docker images / CI
- Commit messages (include .git in zip)
- Clear scalability

#### The whole solution should:
- Be portable, can be built and executed in any system
- Have a clear structure (for both, the API and the Client)
- Be written in NodeJS + Express / Hapi for the backend
- Be easy to grow with new functionality
- Don’t include binaries, .sh files, etc. Use a dependency management tool (npm or yarn).





